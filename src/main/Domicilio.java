package main;

public class Domicilio{
	
	private String calle;
	private int numero;
	private int departamento;
	private int codPostal;
	private String localidad;
	private Provincia provincia;
	
	public Domicilio(String calle, int numero, int departamento, int codPostal, String localidad, Provincia provincia){
		this.calle = calle;
		this.numero = numero;
		this.departamento = departamento;
		this.codPostal = codPostal;
		this.localidad = localidad;
		this.provincia = provincia; //TODO: cambiar a clone
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public int getDepartamento() {
		return departamento;
	}

	public void setDepartamento(int departamento) {
		this.departamento = departamento;
	}

	public int getCodPostal() {
		return codPostal;
	}

	public void setCodPostal(int codPostal) {
		this.codPostal = codPostal;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public Provincia getProvincia() {
		return provincia;
	}

	public void setProvincia(Provincia provincia) {
		this.provincia = provincia;
	}
	
	
	
}
