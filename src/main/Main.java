package main;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Main {

	public static void main(String[] args) {
		List<Provincia> lProvincia= new ArrayList<>();
		lProvincia.add(new Provincia("La Pampa"));
		lProvincia.add(new Provincia("Buenos Aires"));
		lProvincia.add(new Provincia("Tucuman"));
		
		
		Date d1 = new Date(12523);
		Date d2 = new Date(123456789);
		Date d3 = new Date(123);
		
		Domicilio a1 = new Domicilio("Falsa", 1234, 1, 1613, "Springfield", lProvincia.get(0));
		Domicilio a2 = new Domicilio("Lourdes", 14, 40, 1566, "Springfield", lProvincia.get(1));
		Domicilio a3 = new Domicilio("San Martin", 8223, 12, 1586, "Springfield",lProvincia.get(2));
		
		List<Persona> lPersona = new ArrayList<Persona>();
		lPersona.add(new Persona("Leandro", "Funes", d1, a1));
		lPersona.add(new Persona("Lucas", "Addisi", d2, a2));
		lPersona.add(new Persona("Cristian", "Canepa", d3, a3));
		
		List<Persona> deletePersona = new ArrayList<Persona>();
		deletePersona.add(new Persona("Leandro", "Funes", d1, a1));
		deletePersona.add(new Persona("Lucas", "Addisi", d2, a2));
		
		DAO<Persona> storer = new DAONeodatis<Persona>();
		
		for(Persona p : lPersona)
		storer.save(p);
		
		for(Persona p : deletePersona)
		storer.delete(p);
		
	}

}
