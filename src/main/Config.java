package main;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class Config {
	private Config configuration = new Config();
	private Properties prop;
	
	private Config(){
		prop = new Properties();
	}
	
	public Config builder(){
		return configuration; 
	}
	
	public void setProperty(String key, String value){	
		OutputStream output = null;
		try {
		output = new FileOutputStream("config.properties");		
		prop.setProperty(key, value);
		prop.store(output, null);
		} catch (Exception e) {//Deber�a ir fileNotFoundException
			throw new RuntimeException("Exception in setProperty() method,"
					+ " something gone wrong \n" + e.getMessage());
		}finally{
			if (output != null)
				try {
					output.close();
				} catch (IOException e) {
					throw new RuntimeException("Expetion trying to close output stream"
							+ "\n" + e.getMessage());
				}
		}
	}
	
	public String getProperty(String key){
		InputStream input = null;
		String ret = null;
		try{
			input = new FileInputStream("config.properties");
			prop.load(input);
			ret = prop.getProperty(key);
		} catch (Exception e){
			throw new RuntimeException("Exception in getProperty() method,"
					+ " something gone wrong \n" + e.getMessage());
		} finally{
			if (input != null)
				try {
					input.close();
				} catch (IOException e) {
					throw new RuntimeException("Expetion trying to close input stream"
							+ "\n" + e.getMessage());
				}
		}
		return ret;
	}
	
}
