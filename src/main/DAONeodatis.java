package main;

import org.neodatis.odb.ODB;
import org.neodatis.odb.ODBFactory;



public class DAONeodatis<T> implements DAO<T> {

	@Override
	public void save(T t) {
		ODB odb  = ODBFactory.open("DataBase"); //TODO Use properties. Look it up.
		odb.store(t);
		odb.close();
		
	}

	@Override
	public void delete(T t) {
		//FIXME Error
		ODB odb  = ODBFactory.open("DataBase");
		odb.delete(t);
		odb.close();	
	}

}
